
#include "texvisualizer.h"

// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf>
// stl
#include <set>
#include <string>

#include <SOIL.h>

//using namespace GMlib;

//    template <typename T, int n>
    TexVisualizer::TexVisualizer()
      : _no_strips(0), _no_strip_indices(0), _strip_size(0) {

        int width;
        int height;


      initShaderProgram();

      _color_prog.acquire("color");
      assert(_color_prog.isValid());

      _vbo.create();
      _ibo.create();
      //_nmap.create(GL_TEXTURE_2D);

//      _picture.create(GL_TEXTURE_2D);
//      _picture.setName("tex1");
//      _picture.bind();
//      _picture.texParameteri(GL_TEXTURE_WRAP_S, GL_CLAMP);
//      _picture.texParameteri(GL_TEXTURE_WRAP_T, GL_CLAMP);
//      _picture.texParameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//      _picture.texParameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//      _picture.texImage2D(0, GL_RGBA_FLOAT32_ATI, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture);

          // Generate texture name
          glGenTextures(1, &tex);

          // Create texture object
          glBindTexture(GL_TEXTURE_2D, tex);
          unsigned char* image = SOIL_load_image("tex.bmp", &width, &height, 0, SOIL_LOAD_RGB);


          // Specify texture
          glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA_FLOAT32_ATI, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);

          // Set texture parameters
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

          if (0 == image){
              std::cout<<"Error with uploading of image"<< SOIL_last_result()<<std::endl;
          }


    }

    //template <typename T, int n>
    TexVisualizer::TexVisualizer(const TexVisualizer &copy)
   : PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
    {

      initShaderProgram();

      _color_prog.acquire("color");
      assert(_color_prog.isValid());


      _vbo.create();
      _ibo.create();
      //_nmap.create(GL_TEXTURE_2D);
      //_picture.create(GL_TEXTURE_2D);

    }

    //template <typename T, int n>

void TexVisualizer::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

      const GMlib::Camera* cam = renderer->getCamera();
      const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
      const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);
  //    const SqMatrix<float,3> &nmat = obj->getNormalMatrix(cam);

      GMlib::SqMatrix<float,3>        nmat = mvmat.getRotationMatrix();
      nmat.invertOrthoNormal();
      nmat.transpose();

      this->glSetDisplayMode();

      _prog.bind(); {


        // Model view and projection matrices
        //_prog.setUniform( "u_mvmat", mvmat );
        _prog.setUniform( "u_mvpmat", pmat * mvmat );
        //_prog.setUniform( "u_nmat", nmat );
       // _prog.setUniform( "Normal", nmat );
       // _prog.setUniform( "lightVec", nmat );

        // Lights
        _prog.setUniformBlockBinding( "Lights", renderer->getLightUBO(), 0 );

        // Material
        const GMlib::Material &m = obj->getMaterial();
        //_prog.setUniform( "u_mat_amb", m.getAmb() );
        //_prog.setUniform( "u_mat_dif", m.getDif() );
        //_prog.setUniform( "u_mat_spc", m.getSpc() );
       // _prog.setUniform( "u_mat_shi", m.getShininess() );

        // Normal map
        //_prog.setUniform( "u_nmap", _nmap, (GLenum)GL_TEXTURE0, 0 );
        //_prog.setUniform( "tex", tex, (GLenum)GL_TEXTURE0, 0 );

        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "vCoord" );
        GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation( "in_tex" );
        //GMlib::GL::AttributeLocation norm_loc = _prog.getAttributeLocation( "Normal" );

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex);

        GLuint loc = _prog.getUniformLocation("tex")();
        glUniform1i(loc, 0);

        // Bind and draw
        _vbo.bind();
        //_vbo.enable( loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
        _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
        _vbo.enable( tex_loc,  2, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(3*sizeof(GLfloat)) );
        //_vbo.enable( norm_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vert_loc );
        _vbo.disable( tex_loc );
        //_vbo.disable( norm_loc );
        _vbo.unbind();

      } _prog.unbind();



    }

    //template <typename T, int n>

void TexVisualizer::draw() const {

      _ibo.bind();
      for( unsigned int i = 0; i < _no_strips; ++i )
        _ibo.drawElements( GL_TRIANGLE_STRIP, _no_strip_indices, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(i * _strip_size) );
      _ibo.unbind();
    }

    //template <typename T, int n>

void TexVisualizer::replot(
      const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
      const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
      int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
      bool closed_u, bool closed_v
    ) {

      PSurfVisualizer<float,3>::fillStandardVBO( _vbo, p );
      PSurfVisualizer<float,3>::fillTriangleStripIBO( _ibo, p.getDim1(), p.getDim2(), _no_strips, _no_strip_indices, _strip_size );
      //PSurfVisualizer<float,3>::fillNMap( _nmap, normals, closed_u, closed_v );

      //PSurfVisualizer<float,3>::fillNMap( _picture, normals, closed_u, closed_v );
    }

    //template <typename T, int n>

void TexVisualizer::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

      _color_prog.bind(); {
        _color_prog.setUniform( "u_color", color );
        _color_prog.setUniform( "u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()) );
        GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation( "vCoord" );

        _vbo.bind();
        _vbo.enable( vertice_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vertice_loc );
        _vbo.unbind();

      } _color_prog.unbind();

    }

    //template<typename T,int n>
void TexVisualizer::initShaderProgram() {

      const std::string prog_name = "tex_prog";
      if( _prog.acquire(prog_name) ) return;

      std::string vs_src =
          GMlib::GL::OpenGLManager::glslDefHeader150Source() +

          "in vec2 in_tex;\n"
          "in vec4 vCoord;\n"
          "uniform mat4 u_mvpmat;\n"
          "\n"
          //"out vec4 gl_Position;\n"
          "out vec2 ex_tex;\n"
          "\n"
          "void main(void) {\n"
          "\n"
          "  ex_tex = in_tex;\n"
          "  gl_Position = u_mvpmat * vCoord;"
          "}\n"
          ;

      std::string fs_src =
          GMlib::GL::OpenGLManager::glslDefHeader150Source() +
          GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +

          "uniform sampler2D tex;\n"
          "in vec2 ex_tex;\n"
          "\n"
          "void main(void) {\n"
          "\n"
          "  gl_FragColor = texture(tex, ex_tex);\n"
          "}\n"
          ;


      bool compile_ok, link_ok;

      GMlib::GL::VertexShader vshader;
      vshader.create("tex_vs");

      vshader.setPersistent(true);
      vshader.setSource(vs_src);
      compile_ok = vshader.compile();
      if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
      }
      assert(compile_ok);


      GMlib::GL::FragmentShader fshader;
      fshader.create("tex_fs");
      fshader.setPersistent(true);
      fshader.setSource(fs_src);
      compile_ok = fshader.compile();
      if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
      }
      assert(compile_ok);

      _prog.create(prog_name);
      _prog.setPersistent(true);
      _prog.attachShader(vshader);
      _prog.attachShader(fshader);
      link_ok = _prog.link();
      assert(link_ok);

        //std::cout<<"message111"<< std::endl;

    }


GLuint TexVisualizer::getProg()
{
  return _prog.getId();
}







