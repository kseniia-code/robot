#include "bullet.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/ext.hpp"
#include <iostream>

#include <SOIL.h>

Bullet::Bullet()
{

}

Bullet::~Bullet()
{
    delete this;
}

void Bullet::localSimulate(double dt){
    GMlib::Vector<float,3> g( 0.0, 0.0, -9.81);
    v = GMlib::Vector<float,3>( 0.0, -1.3, 0.0);
    this->translate((dt*g + v)*0.2);

}










