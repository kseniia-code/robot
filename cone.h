#ifndef CONE_H
#define CONE_H

#include <windows.h>
#include <iostream>
#include <conio.h>
#include <parametrics/gmpcone>
#include <parametrics/gmpcylinder>
#include <parametrics/gmpbottle8>


class Cone : public GMlib::PCylinder<float> {

public:
  using PCylinder::PCylinder;

  ~Cone(){}
    void move();


protected:

  void localSimulate(double dt);

private:


}; // END class Cone



#endif // CONE_H

