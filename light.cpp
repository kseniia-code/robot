
#include "light.h"

// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf>
// stl
#include <set>
#include <string>


//    template <typename T, int n>
    Light::Light()
      : _no_strips(0), _no_strip_indices(0), _strip_size(0) {

      initShaderProgram();


      _color_prog.acquire("color");
      assert(_color_prog.isValid());

      _vbo.create();
      _vbo2.create();
      _ibo.create();
      _nmap.create(GL_TEXTURE_2D);
    }

    //template <typename T, int n>
    Light::Light(const Light &copy)
   : PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
    {

      glEnable(GL_LIGHTING);
      initShaderProgram();

      _color_prog.acquire("color");
      assert(_color_prog.isValid());

      _vbo.create();
      _vbo2.create();
      _ibo.create();
      _nmap.create(GL_TEXTURE_2D);

      glDisable(GL_LIGHTING);
    }

    //template <typename T, int n>

void Light::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

      const GMlib::Camera* cam = renderer->getCamera();
      const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
      const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);
      //const GMlib::SqMatrix<float,3> &nmat = obj->getNormalMatrix(cam);

      GMlib::SqMatrix<float,3>        nmat = mvmat.getRotationMatrix();
      nmat.invertOrthoNormal();
      nmat.transpose();

      this->glSetDisplayMode();

      _prog.bind(); {


        // Model view and projection matrices
        _prog.setUniform( "u_mvmat", mvmat );
        _prog.setUniform( "u_mvpmat", pmat * mvmat );
        _prog.setUniform( "u_nmat", nmat );
        _prog.setUniform( "Normal", nmat );
        //_prog.setUniform( "lightVec", nmat );

        // Lights
        _prog.setUniformBlockBinding( "Lights", renderer->getLightUBO(), 0 );

        // Material
        const GMlib::Material &m = obj->getMaterial();
        _prog.setUniform( "u_mat_amb", m.getAmb() );
        _prog.setUniform( "u_mat_dif", m.getDif() );
        _prog.setUniform( "u_mat_spc", m.getSpc() );
        _prog.setUniform( "u_mat_shi", m.getShininess() );

//        GMlib::Vector<float,3> a( 0.0, 0.0, 5.0);
//        GMlib::Vector<float,3> b( 0.0, 0.0, 5.0);

//        _prog.setUniform( "light2.position", a );
//        _prog.setUniform( "light2.intensities", b );

        // Normal map
        _prog.setUniform( "u_nmap", _nmap, (GLenum)GL_TEXTURE0, 0 );

        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "vCoord" );
        GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation( "in_tex" );
        GMlib::GL::AttributeLocation norm_loc = _prog.getAttributeLocation( "Normal" );

//        glEnable(GL_LIGHTING);
//        light = glGetUniformLocation(s.getProg(), "lightVec");
//        glUniform3f(light, 100.0, 200.0, 200.0);
//        glDisable(GL_LIGHTING);

        // Bind and draw
        _vbo.bind();

        _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
        _vbo.enable( tex_loc,  2, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(3*sizeof(GLfloat)) );
        _vbo2.bind();
        _vbo2.enable( norm_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLNormal), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vert_loc );
        _vbo.disable( tex_loc );
        _vbo2.disable( norm_loc );
        _vbo.unbind();
        _vbo2.unbind();

      } _prog.unbind();


    }

    //template <typename T, int n>

void Light::draw() const {

      _ibo.bind();
      for( unsigned int i = 0; i < _no_strips; ++i )
        _ibo.drawElements( GL_TRIANGLE_STRIP, _no_strip_indices, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(i * _strip_size) );
      _ibo.unbind();
    }

    //template <typename T, int n>

void Light::replot(
      const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
      const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
      int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
      bool closed_u, bool closed_v
    ) {

      PSurfVisualizer<float,3>::fillStandardVBO( _vbo, p );
      PSurfVisualizer<float,3>::fillTriangleStripIBO( _ibo, p.getDim1(), p.getDim2(), _no_strips, _no_strip_indices, _strip_size );
      PSurfVisualizer<float,3>::fillNMap( _nmap, normals, closed_u, closed_v );

      getNormal(_vbo2, normals);
    }

    //template <typename T, int n>

void Light::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

      _color_prog.bind(); {
        _color_prog.setUniform( "u_color", color );
        _color_prog.setUniform( "u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()) );
        GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation( "vCoord" );

        _vbo.bind();
        _vbo.enable( vertice_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vertice_loc );
        _vbo.unbind();

      } _color_prog.unbind();

    }

    //template<typename T,int n>
void Light::initShaderProgram() {

      const std::string prog_name = "light1_prog";
      if( _prog.acquire(prog_name) ) return;

      std::string vs_src =
          GMlib::GL::OpenGLManager::glslDefHeader150Source() +

        "uniform mat4 u_mvpmat;\n"
        "in vec3 vCoord;\n"
        "in vec2 in_tex;\n"
        "in vec3 Normal;\n"
        "out vec3 fragVert;\n"
        "out vec2 fragTexCoord;\n"
        "out vec3 fragNormal;\n"
        "void main() {\n"
        "    // Pass some variables to the fragment shader\n"
        "    fragTexCoord = in_tex;\n"
         "   fragNormal = Normal;\n"
         "   fragVert = vCoord;\n"
         "   // Apply all matrix transformations to vert\n"
         "   gl_Position = u_mvpmat * vec4(vCoord, 1.0);\n"
        "}\n"

          ;

      std::string fs_src =
          GMlib::GL::OpenGLManager::glslDefHeader150Source() +
          GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +


        "uniform mat3 u_mvmat;\n"
        "uniform mat3 u_nmat;\n"
        "uniform mat4 u_mvpmat;\n"

        "uniform sampler2D tex;\n"
        "struct Light_ {\n"
        "   vec3 position;\n"
        "   vec3 intensities;\n"
        "};"
        "\n"
        "in vec2 fragTexCoord;\n"
        "in vec3 fragNormal;\n"
        "in vec3 fragVert;\n"
        "out vec4 finalColor;\n"
       "Light_ light1 = Light_ (vec3(-20.0, 10.0, 100.0), vec3(1.0, 1.0, 1.0));"
       "Light_ light2 = Light_ (vec3(20.0, 10.0, 100.0), vec3(1.0, 1.0, 1.0));"
       " void main() {\n"
       "     //calculate normal in world coordinates\n"
       "     mat3 normalMatrix = u_nmat;\n"
      //"     mat3 normalMatrix = transpose(inverse(u_mvmat));\n"
        "    vec3 normal = normalize(normalMatrix * fragNormal);\n"
        "    //calculate the location of this fragment (pixel) in world coordinates\n"
        "    vec3 fragPosition = vec3(u_mvpmat * vec4(fragVert, 1.0));\n"
        "    //calculate the vector from this pixels surface to the light source\n"
        "    vec3 surfaceToLight = light1.position - fragPosition;\n"
        "    vec3 surfaceToLight2 = light2.position - fragPosition;\n"
        "    //calculate the cosine of the angle of incidence\n"
        "    float brightness = dot(normal, surfaceToLight) / (length(surfaceToLight) * length(normal));\n"
        "    float brightness2 = dot(normal, surfaceToLight2) / (length(surfaceToLight2) * length(normal));\n"
        "    brightness = clamp(brightness, 0, 1);\n"
        "    brightness2 = clamp(brightness2, 0, 1);\n"
        "    //calculate final color of the pixel, based on:\n"
        "    // The angle of incidence: brightness\n"
        "    // The color/intensities of the light: light.intensities\n"
        "    vec4 surfaceColor = vec4 (0.0, 0.0, 1.0, 1.0);\n"
        "    vec4 surfaceColor2 = vec4 (0.0, 1.0, 0.0, 1.0);\n"
        "    finalColor = vec4(brightness * light1.intensities * surfaceColor.rgb, surfaceColor.a)+vec4(brightness2 * light2.intensities * surfaceColor2.rgb, surfaceColor2.a);\n"
        "}\n"

          ;


      bool compile_ok, link_ok;

      GMlib::GL::VertexShader vshader;
      vshader.create("light1_vs");
      vshader.setPersistent(true);
      vshader.setSource(vs_src);
      compile_ok = vshader.compile();
      if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
      }
      assert(compile_ok);


      GMlib::GL::FragmentShader fshader;
      fshader.create("light1_fs");
      fshader.setPersistent(true);
      fshader.setSource(fs_src);
      compile_ok = fshader.compile();
      if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
      }
      assert(compile_ok);

      _prog.create(prog_name);
      _prog.setPersistent(true);
      _prog.attachShader(vshader);
      _prog.attachShader(fshader);
      link_ok = _prog.link();
      assert(link_ok);

//std::cout<<"message111"<< std::endl;

    }


GLuint Light::getProg()
{
  return _prog.getId();
}

void Light::getNormal(GMlib::GL::VertexBufferObject  &vbo,
                             const GMlib::DMatrix<GMlib::Vector<float, 3> >  &n) {


  GLsizeiptr no_vertices = n.getDim1() * n.getDim2() * sizeof(GMlib::GL::GLNormal);

  vbo.bufferData( no_vertices, 0x0, GL_STATIC_DRAW );
  GMlib::GL::GLNormal *ptr = vbo.mapBuffer<GMlib::GL::GLNormal>();


  for( int i = 0; i < n.getDim1(); i++ ) {
    for( int j = 0; j < n.getDim2(); j++ ) {

      ptr->nx = n(i)(j)(0);
      ptr->ny = n(i)(j)(1);
      ptr->nz = n(i)(j)(2);
      ptr++;
    }
  }
  vbo.unmapBuffer();
}





