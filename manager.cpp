#include "manager.h"
#include "gmlibwrapper.h"

#include "plane.h"
#include "utils.h"
#include "myvisualizer.h"

#include <windows.h>
#include <iostream>
#include <conio.h>

#include <parametrics/gmpsurfpointsvisualizer>

Manager::Manager()
{
    this->toggleVisible();
    insertScene();

}

Manager::~Manager()
{
}



void Manager::insertScene(){

    k1 = 0;
    k2 = 0;
    k3 = 0;
    k4 = 0;
    k5 = 0;

    j = 0;



    //GMlib::Array<Sphere> bullets[10];
    //bullets = new GMlib::Array<Bullet*>;

    //simple geometry
    GMlib::Point<float,3> p0(-10.0, -10.0, 0.0);
    GMlib::Point<float,3> p1(0.55, 0.0, 0.0);
    GMlib::Vector<float,3> v0(0.0, 1.0, 0.0 );
    GMlib::Vector<float,3> v01(0.2, 0.0, 0.0 );
    GMlib::Vector<float,3> v02(0.0, 0.0, 0.2 );
    GMlib::Vector<float,3> v1(50.0, 0.0, 0.0 );
    GMlib::Vector<float,3> v2(0.0, 50.0, 0.0);
    GMlib::Vector<float,3> v3( 0.0, 0.0, 7.5);
    GMlib::Vector<float,3> v4( -15.0, 0.0, 0.0);
    GMlib::Vector<float,3> v5( 0.0, 0.0, 4.0);
    GMlib::Vector<float,3> v6( -1.5, 0.0, 4.0);
    GMlib::Vector<float,3> v7( 1.5, 0.0, 4.0);
    GMlib::Vector<float,3> v8( -1.0, 0.0, 0.7);
    GMlib::Vector<float,3> v9( 1.0, 0.0, 0.7);
    GMlib::Vector<float,3> v10( 30.0, 0.0, 7.0);

    plane_ground = new Plane (p0, v1, v2);
    robot = new Plane (p1, v01, v02);
    sphere = new Sphere (1.5);
    sphere2 = new Sphere (1.5);
    body = new Cone (1, 1, 5);
    arm1 = new Cone (0.5, 0.5, 5);
    arm2 = new Cone (0.5, 0.5, 5);
    foot1 = new Cone (0.5, 0.5, 5);
    foot2 = new Cone (0.5, 0.5, 5);

    //visualizers

    geometry = new Geometry();
    fire = new Fire();
    texvisualizer = new TexVisualizer();
    particles = new Particles();
    //myvisualizer = new MyVisualizer();
    light = new Light();

    robot->toggleDefaultVisualizer();
    //plane->insertVisualizer(surface_visualizer);
    //plane_ground->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    robot->replot(20,10,1,1);

    arm1->insertVisualizer(geometry);
    arm1->replot(20,10,1,1);
    //arm1->setMaterial(GMlib::GMmaterial::Jade);

    arm2->insertVisualizer(geometry);
    arm2->replot(20,10,1,1);
    //arm2->setMaterial(GMlib::GMmaterial::Jade);

    foot1->insertVisualizer(geometry);
    foot1->replot(20,10,1,1);
    //foot1->setMaterial(GMlib::GMmaterial::Jade);

    foot2->insertVisualizer(geometry);
    foot2->replot(20,10,1,1);
    //foot2->setMaterial(GMlib::GMmaterial::Jade);

    //plane_ground->insertVisualizer(phong);

    plane_ground->insertVisualizer(particles);
    //plane_ground->toggleDefaultVisualizer();
    plane_ground->replot(20,10,1,1);
    plane_ground->setMaterial(GMlib::GMmaterial::Silver);

    //sphere->insertVisualizer(phong);
    //sphere->replot(30,10,1,1);
    sphere->insertVisualizer(light);
    sphere->replot(30,10,1,1);


    body->insertVisualizer(texvisualizer);
    body->toggleDefaultVisualizer();
    body->replot(20,10,1,1);
    body->setMaterial(GMlib::GMmaterial::Pearl);

    //sphere2->insertVisualizer(phong);
    //sphere2->insertVisualizer(phong);
    sphere2->insertVisualizer(fire);

    //sphere->insertVisualizer(phong);
    sphere->replot(30,10,1,1);
    //sphere->setMaterial(GMlib::GMmaterial::Turquoise);
    sphere->translate(v3);

    sphere2->replot(20,10,1,1);
    //sphere2->setMaterial(GMlib::GMmaterial::Turquoise);
    sphere2->translate(v10);


    plane_ground->translate(v4);
    body->translate(v5);

    arm1->translate(v6);
    arm2->translate(v7);
    //arm1->rotateParent(GMlib::Angle(30), v0);
    //arm2->rotateParent(GMlib::Angle(-30), v0);

    foot1->translate(v8);
    foot2->translate(v9);
    foot1->rotateParent(GMlib::Angle(90), v01);
    foot2->rotateParent(GMlib::Angle(-90), v01);

    //robot->translate(v7);

    //std::cout<<"message"<< std::endl;
    this->insert(plane_ground);
    //this->insert(sphere2);

    robot->insert(sphere);
    robot->insert(body);
    //body->insert(arm1);
    //body->insert(arm2);
    robot->insert(arm1);
    robot->insert(arm2);
    robot->insert(foot1);
    robot->insert(foot2);
    this->insert(robot);
    //this->insert(saucer);


//    this->insert(sphere);
//    this->insert(body);
//    this->insert(arm1);
//    this->insert(arm2);
//    this->insert(foot1);
//    this->insert(foot2);

    this->setSurroundingSphere(GMlib::Sphere<float, 3>(100));
}

void Manager::moveLeftA(){

    GMlib::Vector<float,3> vUp( -2.0, 0.0, 0.0);
    GMlib::Vector<float,3> vL( 0.0, 0.0, -1.5);

    GMlib::Vector<float,3> vDown( 0.0, 0.0, -2.0);
    GMlib::Vector<float,3> vR( 1.5, 0.0, 0.0);

    GMlib::Vector<float,3> v0(0.0, 1.0, 0.0 );
    GMlib::Point<float,3> p(-5.5, 0.0, 4.0);


    if (k1<1){
    arm1->rotateParent(GMlib::Angle(90), v0);
    //arm1->rotate(GMlib::Angle(70), p, v0);
    arm1->translate(vUp);
    arm1->translate(vL);
    k1++;
    }
    else{

        arm1->rotateParent(GMlib::Angle(-90), v0);
        //arm1->rotate(GMlib::Angle(-70), p, v0);

        arm1->translate(vDown);
        arm1->translate(vR);
       k1--;

    }

}


void Manager::moveRightA(){

    GMlib::Vector<float,3> vUp( 2.0, 0.0, 0.0);
    GMlib::Vector<float,3> vL( 0.0, 0.0, -1.5);

    GMlib::Vector<float,3> vDown( 0.0, 0.0, -2.0);
    GMlib::Vector<float,3> vR( -1.5, 0.0, 0.0);

    GMlib::Vector<float,3> v( 0.3, 0.0, 0.0);
    GMlib::Vector<float,3> v0(0.0, 1.0, 0.0 );
    GMlib::Point<float,3> p(0.5, 0.0, 4.0);

    if (k2<1){
    arm2->rotateParent(GMlib::Angle(-90), v0);
    //arm2->rotate(GMlib::Angle(-70), p, v0);
    arm2->translate(vUp);
    arm2->translate(vL);
    k2++;
    }
    else{

        arm2->rotateParent(GMlib::Angle(90), v0);
        //arm2->rotate(GMlib::Angle(70), p, v0);
        arm2->translate(vDown);
        arm2->translate(vR);
       k2--;

    }
}

void Manager::moveLeftF(){

    GMlib::Vector<float,3> v( 0.0, -0.2, 0.0);
    GMlib::Vector<float,3> v0(1.0, 0.0, 0.0);

    GMlib::Point<float,3> p(0.5, 0.0, 0.0);

    if (k3<1){
    //arm1->rotateParent(GMlib::Angle(50), v0);
    foot1->rotate(GMlib::Angle(-90), p, v0);
    //arm1->translate(vUp);
    k3++;
    }
    else{

        //arm1->rotateParent(GMlib::Angle(-50), v0);
        foot1->rotate(GMlib::Angle(90), p, v0);
        //arm1->translate(vDown);
       k3--;

    }

}

void Manager::moveRightF(){

    GMlib::Vector<float,3> v( 0.0, 0.2, 0.0);
    GMlib::Vector<float,3> v0(1.0, 0.0, 0.0 );
    GMlib::Point<float,3> p(0.5, 0.0, 0.0);

    if (k4<1){
    //arm1->rotateParent(GMlib::Angle(50), v0);
    foot2->rotate(GMlib::Angle(90), p, v0);
    //arm1->translate(vUp);
    k4++;
    }
    else{

        //arm1->rotateParent(GMlib::Angle(-50), v0);
        foot2->rotate(GMlib::Angle(-90), p, v0);
        //arm1->translate(vDown);
       k4--;

    }

}


void Manager::moveRobot(){

    GMlib::Vector<float,3> v( 0.0, -0.5, 0.0);
    GMlib::Vector<float,3> v0(1.0, 0.0, 0.0 );
    GMlib::Point<float,3> p(0.5, 0.0, 0.0);

    robot->translate(v);

    if (k5<1){
        foot1->rotate(GMlib::Angle(-20), p, v0);
        foot2->rotate(GMlib::Angle(20), p, v0);
    k5++;
    }
    else{

        foot1->rotate(GMlib::Angle(20), p, v0);
        foot2->rotate(GMlib::Angle(-20), p, v0);
       k5--;

    }
}

void Manager::rotateRobot(){

    //GMlib::Vector<float,3> v( 0.0, -0.5, 0.0);
    GMlib::Vector<float,3> v0(0.0, 0.0, 1.0 );
    GMlib::Point<float,3> p(0.5, 0.0, 0.0);

//    GMlib::Vector<float,3> v1(cos(-30), -sin(-30), 0.0);
//    GMlib::Vector<float,3> v2(sin(-30), cos(-30), 0.0);
//    GMlib::Vector<float,3> v3(0.0, 0.0, 1.0);

//    GMlib::Vector<GMlib::Vector<float,3>,3> m(v1, v2, v3);

//    GMlib::Matrix<float, 3, 3> rot_mat(m);

    robot->rotate(GMlib::Angle(-30), p, v0);

    //find vector of rotation
    //rot_v = (robot->getMatrixToSceneInverse())*rot_v;
    //rot_v = (rot_mat*rot_v);
    //rot_v.normalize();
    //rot_v = rot_v*0.4;
    //std::cout<<rot_v<< std::endl;

}

void Manager::shootRobot(){

    GMlib::Vector<float,3> v( 0.0, -0.5, 0.0);
    GMlib::Vector<float,3> v0(0.0, 0.0, 1.0 );
    GMlib::Vector<float,3> vA(0.0, 0.0, 4.0 );
    GMlib::Point<float,3> p(0.5, 0.0, 0.0);

    bullet = new Bullet(0.3);
    bullet->translate(body->getPos());
    bullet->toggleDefaultVisualizer();
    bullet->replot(20,10,1,1);

    bullet->setMaterial(GMlib::GMmaterial::PolishedGold);
    robot->insert(bullet);

    bullets.insertFront(bullet);

    shoot = true;

}

void Manager::localSimulate(double dt) {

    //check coordinates of bullet
    //cam = wrapper->getCam();
    //const GMlib::HqMatrix<float,3> &m_b = bullet->getModelViewMatrix(cam);
    //const GMlib::HqMatrix<float,3> &m_g = plane_ground->getModelViewMatrix(cam);

    if (shoot == true){

    //GMlib::Point<float,3> b_pos = bullet->getGlobalPos();
    //GMlib::Point<float,3> p_pos = plane_ground->getGlobalPos();

    //std::cout<<p_pos<< std::endl;
    //if (m_b[2][2] < m_g[2][2]){3


        for (int i = 0; i < bullets.size(); i++){
        if (  bullets[i]->getGlobalPos()(2) < plane_ground->getGlobalPos()(2)+2.1){

        //std::cout<<"111"<< std::endl;
        bullets[i]->insertVisualizer(fire);
        //bullets[i]->replot(20,10,1,1);
        }
        }

    for (int i = 0; i < bullets.size(); i++){
    if (  bullets[i]->getGlobalPos()(2) < plane_ground->getGlobalPos()(2)+2){

    //std::cout<<"111"<< std::endl;
    // robot->remove(bullet);
     bullets.remove(bullets[i]);
     this->remove(bullets[i]);
    }
    }

    }





}


