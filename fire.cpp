
#include "fire.h"

// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf>
// stl
#include <set>
#include <string>

#include <SOIL.h>

//using namespace GMlib;


//    template <typename T, int n>
    Fire::Fire()
      : _no_strips(0), _no_strip_indices(0), _strip_size(0) {

        int width;
        int height;


      initShaderProgram();

      _color_prog.acquire("color");
      assert(_color_prog.isValid());

      _vbo.create();
      _ibo.create();
      //_nmap.create(GL_TEXTURE_2D);

    }

    //template <typename T, int n>
    Fire::Fire(const Fire &copy)
   : PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
    {

      initShaderProgram();

      _color_prog.acquire("color");
      assert(_color_prog.isValid());


      _vbo.create();
      _ibo.create();
      //_nmap.create(GL_TEXTURE_2D);

    }

    //template <typename T, int n>

void Fire::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

      const GMlib::Camera* cam = renderer->getCamera();
      const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
      const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);
  //    const SqMatrix<float,3> &nmat = obj->getNormalMatrix(cam);

      GMlib::SqMatrix<float,3>        nmat = mvmat.getRotationMatrix();
      nmat.invertOrthoNormal();
      nmat.transpose();

      this->glSetDisplayMode();

      _prog.bind(); {


        // Model view and projection matrices
        //_prog.setUniform( "u_mvmat", mvmat );
        _prog.setUniform( "u_mvpmat", pmat * mvmat );
        //_prog.setUniform( "u_nmat", nmat );
       // _prog.setUniform( "Normal", nmat );
       // _prog.setUniform( "lightVec", nmat );

        // Lights
        _prog.setUniformBlockBinding( "Lights", renderer->getLightUBO(), 0 );

        // Material
        const GMlib::Material &m = obj->getMaterial();
        //_prog.setUniform( "u_mat_amb", m.getAmb() );
        //_prog.setUniform( "u_mat_dif", m.getDif() );
        //_prog.setUniform( "u_mat_spc", m.getSpc() );
       // _prog.setUniform( "u_mat_shi", m.getShininess() );

        // Normal map
        //_prog.setUniform( "u_nmap", _nmap, (GLenum)GL_TEXTURE0, 0 );
        //_prog.setUniform( "tex", tex, (GLenum)GL_TEXTURE0, 0 );

        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "vCoord" );
        GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation( "in_tex" );
        //GMlib::GL::AttributeLocation norm_loc = _prog.getAttributeLocation( "Normal" );

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, tex);

        GLuint loc = _prog.getUniformLocation("tex")();
        glUniform1i(loc, 0);

        // Bind and draw
        _vbo.bind();
        //_vbo.enable( loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
        _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
        _vbo.enable( tex_loc,  2, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(3*sizeof(GLfloat)) );
        //_vbo.enable( norm_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vert_loc );
        _vbo.disable( tex_loc );
        //_vbo.disable( norm_loc );
        _vbo.unbind();

      } _prog.unbind();



    }

    //template <typename T, int n>

void Fire::draw() const {

      _ibo.bind();
      for( unsigned int i = 0; i < _no_strips; ++i )
        _ibo.drawElements( GL_TRIANGLE_STRIP, _no_strip_indices, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(i * _strip_size) );
      _ibo.unbind();
    }

    //template <typename T, int n>

void Fire::replot(
      const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
      const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
      int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
      bool closed_u, bool closed_v
    ) {

      PSurfVisualizer<float,3>::fillStandardVBO( _vbo, p );
      PSurfVisualizer<float,3>::fillTriangleStripIBO( _ibo, p.getDim1(), p.getDim2(), _no_strips, _no_strip_indices, _strip_size );
      //PSurfVisualizer<float,3>::fillNMap( _nmap, normals, closed_u, closed_v );

      //PSurfVisualizer<float,3>::fillNMap( _picture, normals, closed_u, closed_v );
    }

    //template <typename T, int n>

void Fire::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

      _color_prog.bind(); {
        _color_prog.setUniform( "u_color", color );
        _color_prog.setUniform( "u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()) );
        GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation( "vCoord" );

        _vbo.bind();
        _vbo.enable( vertice_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vertice_loc );
        _vbo.unbind();

      } _color_prog.unbind();

    }

    //template<typename T,int n>
void Fire::initShaderProgram() {

      const std::string prog_name = "fire_prog";
      if( _prog.acquire(prog_name) ) return;

      std::string vs_src =
          GMlib::GL::OpenGLManager::glslDefHeader150Source() +

          "in vec2 in_tex;\n"
          "in vec4 vCoord;\n"
          //"in vec4 in_Colour;\n"
          "uniform mat4 u_mvpmat;\n"
          "\n"
          "out vec4 gl_Position;\n"
          "smooth out vec2 ex_tex;\n"
          "out vec4 v_color;\n"
          "\n"
          "void main(void) {\n"
          "\n"
          "  ex_tex = in_tex;\n"
          "  gl_Position = u_mvpmat * vCoord;\n"
          "  v_color = vec4(1.0, 0.0, 0.0, 1.0);\n"
          "}\n"
          ;

      std::string fs_src =
          GMlib::GL::OpenGLManager::glslDefHeader150Source() +
          GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +

          "uniform sampler2D tex;\n"
          "smooth in vec2 ex_tex;\n"
          "uniform float time;\n"
          "in vec4 v_color;\n"
          "\n"
          "    void main()\n"
          "    {\n"
          "    vec2 p = -0.5 + ex_tex.xy;\n"
          "    float Color = 3.0 - (2.0*length(2.0*p));\n"
          "    vec3 coord = vec3(atan(p.x,p.y)/6.2832+0.5, length(p)*0.4, 0.5);\n"
          "    for(int i = 2; i <= 7; i++)              {\n"
          "    float power = pow(2.0, float(i));\n"
          "    vec3 uv = coord + vec3(0.,time*.05, time*.01);\n"
          "    float res = power*16.0;\n"
          "    const vec3 s = vec3(1e0, 1e2, 1e4);\n"
          "      uv *= res;\n"
          "    vec3 uv0 = floor(mod(uv, res))*s;\n"
          "    vec3 uv1 = floor(mod(uv+vec3(1.), res))*s;\n"
          "    vec3 f = fract(uv); f = f*f*(3.0-2.0*f);\n"
          "    vec4 v = vec4(uv0.x+uv0.y+uv0.z, uv1.x+uv0.y+uv0.z,\n"
          "          uv0.x+uv1.y+uv0.z, uv1.x+uv1.y+uv0.z);\n"
          "    vec4 r = fract(sin(v*1e-3)*1e5);\n"
          "    float r0 = mix(mix(r.x, r.y, f.x), mix(r.z, r.w, f.x), f.y);\n"
          "    r = fract(sin((v + uv1.z - uv0.z)*1e-3)*1e5);\n"
          "    float r1 = mix(mix(r.x, r.y, f.x), mix(r.z, r.w, f.x), f.y);\n"
          "    float snoise = mix(r0, r1, f.z)*2.-1.0;\n"
          "        Color += (1.5 / power) * snoise;\n"
          "    }\n"
          "    gl_FragColor = vec4( Color, pow(max(Color,0.),2.)*0.4, pow(max(Color,0.),3.)*0.15 , 1.0);\n"
          "    }\n"
          ;


      bool compile_ok, link_ok;

      GMlib::GL::VertexShader vshader;
      vshader.create("fire_vs");
      vshader.setPersistent(true);
      vshader.setSource(vs_src);
      compile_ok = vshader.compile();
      if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
      }
      assert(compile_ok);


      GMlib::GL::FragmentShader fshader;
      fshader.create("fire_fs");
      fshader.setPersistent(true);
      fshader.setSource(fs_src);
      compile_ok = fshader.compile();
      if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
      }
      assert(compile_ok);

      _prog.create(prog_name);
      _prog.setPersistent(true);
      _prog.attachShader(vshader);
      _prog.attachShader(fshader);
      link_ok = _prog.link();
      assert(link_ok);

    }


GLuint Fire::getProg()
{
  return _prog.getId();
}







