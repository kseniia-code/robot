#ifndef SPHERE_H
#define SPHERE_H

#include <parametrics/gmpsphere>


class Sphere : public GMlib::PSphere<float> {

public:
  using PSphere::PSphere;

    Sphere();
    ~Sphere(){}
    int t;

protected:
    void privateInit();
    void privateRender();
    void privateUpdate();
    void localSimulate(double dt);

private:


}; // END class Sphere



#endif // SPHERE_H

