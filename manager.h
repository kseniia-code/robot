#ifndef MANAGER_H
#define MANAGER_H

//class MyVisualizer;
//class Cone;
//class Sphere;
class GMlibWrapper;

#include <windows.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <core/gmarray>
#include <parametrics/gmpcylinder>
#include "sphere.h"
#include "cone.h"
#include "myvisualizer.h"
#include "texvisualizer.h"
#include "geometry.h"
#include "plane.h"
#include "particles.h"
#include "bullet.h"
#include "camera.hpp"
#include "fire.h"
#include "light.h"


class Manager : public GMlib::SceneObject {
    GM_SCENEOBJECT(Manager)

	public:
		Manager();
		~Manager();

        void moveLeftA();
        void moveRightA();
        void moveLeftF();
        void moveRightF();
        void moveRobot();
        void rotateRobot();
        void shootRobot();
        void insertScene();

    

  protected:

        void localSimulate(double dt);

	private:
        MyVisualizer*                                     myvisualizer;
        TexVisualizer*                                    texvisualizer;
        Geometry*                                         geometry;
        Light*                                            light;
        Particles*                                        particles;
        Cone*                                             arm1;
        Cone*                                             arm2;
        Cone*                                             body;
        Cone*                                             foot1;
        Cone*                                             foot2;
        Sphere*                                           sphere;
        Sphere*                                           sphere2;
        Bullet*                                           bullet;
        Plane*                                            plane_ground;
        Plane*                                            robot;
        int                                               k1, k2, k3, k4, k5, j;
        bool                                              shoot;
        Fire*                                             fire;

        GMlib::Array<Bullet*>                             bullets;
        GMlib::Camera* cam;
        GMlibWrapper* wrapper;


};

#endif // MANAGER_H

