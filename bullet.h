#ifndef BULLET_H
#define BULLET_H

#include <parametrics/gmpsphere>

class Bullet : public GMlib::PSphere<float> {

public:
  using PSphere::PSphere;

    Bullet();
    ~Bullet();

protected:
    void localSimulate(double dt);

private:

    GMlib::Vector<float,3>                            v;



}; // END class Bullet



#endif // BULLET_H

