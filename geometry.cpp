
#include "geometry.h"

// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>
#include <parametrics/gmpsurf>
// stl
#include <set>
#include <string>



//using namespace GMlib;

//    template <typename T, int n>
    Geometry::Geometry()
      : _no_strips(0), _no_strip_indices(0), _strip_size(0) {

      initShaderProgram();


      _color_prog.acquire("color");
      assert(_color_prog.isValid());

      _vbo.create();
      _ibo.create();
      _nmap.create(GL_TEXTURE_2D);
    }

    //template <typename T, int n>
    Geometry::Geometry(const Geometry &copy)
   : PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0)
    {

      initShaderProgram();

      _color_prog.acquire("color");
      assert(_color_prog.isValid());


      _vbo.create();
      _ibo.create();
      _nmap.create(GL_TEXTURE_2D);
    }

    //template <typename T, int n>

void Geometry::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

      const GMlib::Camera* cam = renderer->getCamera();
      const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
      const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);
  //    const SqMatrix<float,3> &nmat = obj->getNormalMatrix(cam);

      GMlib::SqMatrix<float,3>        nmat = mvmat.getRotationMatrix();
      nmat.invertOrthoNormal();
      nmat.transpose();

      this->glSetDisplayMode();

      _prog.bind(); {


        // Model view and projection matrices
        //_prog.setUniform( "u_mvmat", mvmat );
        _prog.setUniform( "u_mvpmat", pmat * mvmat );
        //_prog.setUniform( "u_nmat", nmat );
       // _prog.setUniform( "Normal", nmat );
       // _prog.setUniform( "lightVec", nmat );

        // Lights
        _prog.setUniformBlockBinding( "Lights", renderer->getLightUBO(), 0 );

        // Material
        const GMlib::Material &m = obj->getMaterial();
        //_prog.setUniform( "u_mat_amb", m.getAmb() );
        //_prog.setUniform( "u_mat_dif", m.getDif() );
        //_prog.setUniform( "u_mat_spc", m.getSpc() );
       // _prog.setUniform( "u_mat_shi", m.getShininess() );

        // Normal map
        //_prog.setUniform( "u_nmap", _nmap, (GLenum)GL_TEXTURE0, 0 );

        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "vCoord" );
        //GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation( "in_tex" );
        //GMlib::GL::AttributeLocation norm_loc = _prog.getAttributeLocation( "Normal" );

        // Bind and draw
        _vbo.bind();
        _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
        //_vbo.enable( tex_loc,  2, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(3*sizeof(GLfloat)) );
        //_vbo.enable( norm_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vert_loc );
        //_vbo.disable( tex_loc );
        //_vbo.disable( norm_loc );
        _vbo.unbind();

      } _prog.unbind();


    }

    //template <typename T, int n>

void Geometry::draw() const {

      _ibo.bind();
      for( unsigned int i = 0; i < _no_strips; ++i )
        _ibo.drawElements( GL_TRIANGLE_STRIP, _no_strip_indices, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(i * _strip_size) );
      _ibo.unbind();
    }

    //template <typename T, int n>

void Geometry::replot(
      const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
      const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
      int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
      bool closed_u, bool closed_v
    ) {

      PSurfVisualizer<float,3>::fillStandardVBO( _vbo, p );
      PSurfVisualizer<float,3>::fillTriangleStripIBO( _ibo, p.getDim1(), p.getDim2(), _no_strips, _no_strip_indices, _strip_size );
      PSurfVisualizer<float,3>::fillNMap( _nmap, normals, closed_u, closed_v );
    }

    //template <typename T, int n>

void Geometry::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

      _color_prog.bind(); {
        _color_prog.setUniform( "u_color", color );
        _color_prog.setUniform( "u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()) );
        GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation( "vCoord" );

        _vbo.bind();
        _vbo.enable( vertice_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

        draw();

        _vbo.disable( vertice_loc );
        _vbo.unbind();

      } _color_prog.unbind();

    }

    //template<typename T,int n>
void Geometry::initShaderProgram() {

      const std::string prog_name = "g_prog";
      if( _prog.acquire(prog_name) ) return;

      std::string vs_src =
          GMlib::GL::OpenGLManager::glslDefHeader150Source() +


          "uniform mat4 u_mvpmat;\n"
          "in vec4 vCoord;\n"
          "out vec3 vColor;\n"
          //"\n"
          //"out vec4 gl_Position;\n"
          "\n"
          "void main(void) {\n"
          "\n"
          "  gl_Position = u_mvpmat * vCoord;"
          "  vColor = vec3(0.0, 0.0, 1.0);"
          "}\n"
          ;

      std::string gs_src =
          GMlib::GL::OpenGLManager::glslDefHeader150Source() +

              "layout(triangles) in;\n"
              "layout(triangle_strip, max_vertices = 3) out;\n"
              "in vec3 vColor[];\n"
              "out vec3 fColor;\n"
              "const float PI = 3.1415926;"
              "\n"
              "void main(){\n"
              "fColor = vColor[0];\n"
              "for(int i=0; i<3; i++)\n"
              "{\n"
              //"float ang = PI * 2.0 / 10.0 * i;\n"
              // Angle between each side in radians
              "float ang = PI / 4 * i;\n"
              // Offset from center of point
              "vec4 offset = vec4(cos(ang), -sin(ang), 0.0, 0.0);\n"
              //"vec4 offset = vec4(cos(ang) * 0.3, -sin(ang) * 0.4, 0.0, 0.0);\n"
              "gl_Position = gl_in[i].gl_Position + offset;\n"
              "EmitVertex();}\n"
              "EndPrimitive();"
              "}\n"
          ;



      std::string fs_src =
          GMlib::GL::OpenGLManager::glslDefHeader150Source() +
          GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +

          "in vec3 fColor;\n"
          "out vec4 outColor;\n"
          "void main(void) {\n"
          "\n"
          "  //gl_FragColor = vec4(0.0, 1.0, 1.0, 1.0);\n"
             "outColor = vec4(fColor, 1.0);\n"
          "}\n"
          ;


      bool compile_ok, link_ok;

      GMlib::GL::VertexShader vshader;
      vshader.create("g_vs");
//std::cout<<"message111"<< std::endl;
      vshader.setPersistent(true);
      vshader.setSource(vs_src);
      compile_ok = vshader.compile();
      if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
      }
      assert(compile_ok);

      GMlib::GL::GeometryShader gshader;
      gshader.create("g_gs");
      gshader.setPersistent(true);
      gshader.setSource(gs_src);
      compile_ok = gshader.compile();
      if( !compile_ok ) {
        std::cout << "Src:" << std::endl << gshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << gshader.getCompilerLog() << std::endl;
      }
      assert(compile_ok);


      GMlib::GL::FragmentShader fshader;
      fshader.create("g_fs");
      fshader.setPersistent(true);
      fshader.setSource(fs_src);
      compile_ok = fshader.compile();
      if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
      }
      assert(compile_ok);

      _prog.create(prog_name);
      _prog.setPersistent(true);
      _prog.attachShader(vshader);
      _prog.attachShader(gshader);
      _prog.attachShader(fshader);      
      link_ok = _prog.link();
      assert(link_ok);



    }


GLuint Geometry::getProg()
{
  return _prog.getId();
}







